
app.controller('userController', function($scope,appService,$routeParams) {
$scope.fName = '';
$scope.lName = '';
$scope.passw1 = '';
$scope.passw2 = '';
$scope.email = '';
$scope.address = '';


$scope.users = appService.getUserList();
$scope.edit = true;
$scope.error = false;
$scope.incomplete = false;

  if ($routeParams.id != null) {
    console.log(">$scope.users[$routeParams.id-1]",$scope.users[$routeParams.id-1]);
    $scope.id=$routeParams.id;
    $scope.addEditHeading="Edit User";
    $scope.edit = false;
    $scope.fName = $scope.users[$routeParams.id-1].fName;
    $scope.lName = $scope.users[$routeParams.id-1].lName; 
    $scope.passw1 = $scope.users[$routeParams.id-1].passw1;
    $scope.passw2 = $scope.users[$routeParams.id-1].passw2;
    $scope.email = $scope.users[$routeParams.id-1].email; 
    $scope.address = $scope.users[$routeParams.id-1].address; 



  } else {
      $scope.addEditHeading="Add New User";
  }
$scope.goAddUserView=function(){
  window.location="#/add-user";
}
$scope.goEditUser = function(id) {
  window.location="#/edit-user/"+id;
};

$scope.editUser = function(id) {
  if (id == 'new') {
    $scope.edit = true;
    $scope.incomplete = true;
    $scope.fName = '';
    $scope.lName = '';
    $scope.passw1 = '';
    $scope.email = '';
    $scope.address = '';


    } else {
      console.log(">$scope.users[id-1]",$scope.users[id-1]);
    $scope.edit = false;
    $scope.fName = $scope.users[id-1].fName;
    $scope.lName = $scope.users[id-1].lName;
    $scope.passw1 = $scope.users[id-1].passw1;
    $scope.email = $scope.users[id-1].email;
    $scope.address = $scope.users[id-1].address;
  }
};

$scope.goListView=function(){
  window.location="#/list-user";
};
$scope.addEditUser=function(){
  console.log("array-length:"+$scope.users.length);
  if($scope.id==null){ //New user 
      $scope.users[$scope.users.length]={
      id:$scope.users.length+1,
      fName:$scope.fName,
      lName:$scope.lName,
      passw1:$scope.passw1,
      email:$scope.email,
      address:$scope.address
    };
  }else{ //update user
    var user=new Object();
    user.id=$scope.id;
    user.fName=$scope.fName;
    user.lName=$scope.lName;
    user.passw1=$scope.passw1;
    user.email=$scope.email;
    user.address=$scope.address;
    appService.updateUser(user);
  }
  appService.setUserList($scope.users);
  window.location="#/list-user";
  //console.log("array:"+JSON.stringify($scope.users));
};

$scope.$watch('email',function() {$scope.test();});
$scope.$watch('address',function() {$scope.test();});
$scope.$watch('passw1',function() {$scope.test();});
$scope.$watch('passw2',function() {$scope.test();});
$scope.$watch('fName', function() {$scope.test();});
$scope.$watch('lName', function() {$scope.test();});

$scope.test = function() {
  if ($scope.passw1 !== $scope.passw2) {
    $scope.error = true;
    } else {
    $scope.error = false;
  }
  $scope.incomplete = false;
  if ($scope.edit && (!$scope.fName.length ||
  !$scope.lName.length ||
  !$scope.passw1.length || 
  !$scope.passw2.length ||
  !$scope.email.length || 
  !$scope.address.length)) {
     $scope.incomplete = true;
  }
};
});