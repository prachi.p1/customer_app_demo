/* Generic Services */                                                                                                                                                                                                    
 angular.module('app.service', [])                                                                                                                                                                        
   .factory("appService", function($http) {
   	 var user = null;
     var userList = [
    {id:1, fName:'Hege', lName:"Pege", passw1:"123456", passw2:"123456", email:"hege123@gmail.com", address:"India" },
    {id:2, fName:'Kim',  lName:"Pim", passw1:"123456", passw2:"123456",email:"kim123@gmail.com", address:"Australia"  },
    {id:3, fName:'Sal',  lName:"Smith", passw1:"123456", passw2:"123456",email:"sal123@gmail.com", address:"Chicago"  },
    {id:4, fName:'Jack', lName:"Jones", passw1:"123456", passw2:"123456",email:"jack123@gmail.com", address:"India"  },
    {id:5, fName:'John', lName:"Doe", passw1:"123456" , passw2:"123456" , email:"john123@gmail.com", address:"America" },
    {id:6, fName:'Peter',lName:"Pan", passw1:"sample" , passw2:"sample" ,email:"peter123@gmail.com", address:"Canada" }
    ];                                                                                                                                                
     return {                                                                                                                                                                                                              
        doSomething: function() {
       	  console.log("doSomething");
        },
        setUserList: function(list) {
          userList=list;
        },
        getUserList: function() {
          return userList;
        },        
        updateUser: function(currentUser) {
          if(currentUser!=null && currentUser.id!=null){
            // list is started from index 0
            var listIndex=currentUser.id-1;
           // console.log("check:"+JSON.stringify(userList[listIndex]));
            userList[listIndex].fName=currentUser.fName;
            userList[listIndex].lName=currentUser.lName;
            userList[listIndex].password=currentUser.password;
            userList[listIndex].email=currentUser.email;
            userList[listIndex].address=currentUser.address;
          }
        }
    }
});