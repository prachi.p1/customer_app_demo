import logo from './logo.svg';
import './App.css';
import { FuncComp } from './component/FuncComp';
import CustomerFunc from './component/CustomerFunc ';
import { BrowserRouter as Router, Routes, Route, Link, Navigate } from 'react-router-dom';
import Home from './containers/Home';
import Login from './containers/Login';
import About from './containers/About';
import PageNotFound from './containers/PageNotFound';
import CustomerAdd from './component/CustomerAdd';
import CustomerList from './component/CustomerList';

function App() {
  // return (
  //   <div className="App">
  //     <CustomerFunc/>
  //   </div>
  // );

  // const App = ()=>{
  return (
    <Router>
      <div style={{ marginLeft: '10px', marginRight: '10px' }}>
        <h2> Customer App</h2>
        <Link to={'/home'}>Home</Link> |
        <Link to={'/about'}>About</Link> |
        <Link to={'/login'}>Logout</Link> |
        
        <hr />
        <Routes>
          <Route exact path="/" element={<Navigate to={{ pathname: "/login" }} />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/about" element={<About />} />
          <Route exact path="/home" element={<Home />} />
          
          <Route exact path="/customer/add" element={<CustomerAdd />} />
          <Route exact path="/customer/edit/:recordId" element={<CustomerAdd />} />
          <Route exact path="/customer/list" element={<CustomerList />} />
          {/* <Route exact path="/customer" element={<CustomerApp />} /> */}
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </div>
    </Router>
  );
}
export default App;
