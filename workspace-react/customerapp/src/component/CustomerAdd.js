import React,{useEffect, useState} from 'react';
// import ReactDOM from 'react-dom';
import { addCustomer, deleteCustomer, getCustomers, updateCustomer,getCustomerById } from '../services/CustomerData';
import { useNavigate, useParams } from 'react-router';

export function CustomerAdd () {
    const navigate = useNavigate();
    const [items,setItems] = useState(getCustomers());
    const [customer,setCustomer] = useState({id:0, name: '',email:'',phone:'',address:''});
    const [buttonLabel,setButtonLabel] = useState('Add');
  
  let { recordId } = useParams();
  const loadCustomer = (id)=> {
    console.log(">> loadCustomer :"+id);
    let customerSelect  = getCustomerById(id);
    setCustomer({...customerSelect});
    setButtonLabel("Update");
  }

  useEffect(()=>{
      if(recordId){
        loadCustomer(recordId);
      }
  },[])

  const generateId=()=>{
      return items.length +1;
  }
  const reset=()=>{
    setCustomer({
        name: '',
        email: '',
        phone: '',
        address: '',
        id: 0,
      });
    setButtonLabel("Add");
  }

  const handleChange = (e) => {
    setCustomer({...customer, [e.target.name]: e.target.value });
  }
  const doEdit=(id)=>{
    let tempArray = items.filter((item)=>(item.id == id));
    if(tempArray.length > 0){
        let {id,name,email,phone,address} = tempArray[0];
        setCustomer({
            id,name,email,phone,address
        });
        setButtonLabel('Update');
        }
    }

 const handleSubmit = (e) => {
    e.preventDefault();
    if (!customer.name.length) {
      return;
    }
    //const newItem = customer;
    const newItem = {...customer}; //new Object
    if(customer.id != 0){ //edit
        newItem.id = customer.id;
        updateCustomer(newItem);
    }else{//add
        newItem.id = generateId();
        addCustomer(newItem);
    }
    navigate('/customer/list');
  }
    return (
      <div>
        <h3>Customer App Func</h3>
        <form onSubmit={handleSubmit}>
          <input
            placeholder='Name'
            name='name'
            onChange={handleChange}
            value={customer.name}
          /><br/><br/>
          <input
            placeholder='Email'
            name='email'
            onChange={handleChange}
            value={customer.email}
          /><br/><br/>
          <input
            placeholder='Phone'
            name='phone'
            onChange={handleChange}
            value={customer.phone}
          /><br/><br/>
          <input
            placeholder='Address'
            name='address'
            onChange={handleChange}
            value={customer.address}
          /><br/><br/>
          <button>{buttonLabel}</button> &nbsp;&nbsp;
          <input type='button' onClick={reset} value="Cancel" /> 
        </form>
      </div>
    );
}


export default CustomerAdd;