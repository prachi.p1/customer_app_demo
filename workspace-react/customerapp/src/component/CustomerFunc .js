import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-date-picker';
import { addCustomer, deleteCustomer, getCustomers, updateCustomer } from '../services/CustomerData';

export function CustomerFunc() {
    const [items, setItems] = useState(getCustomers());
    const [customer, setCustomer] = useState({ id: 0, dob: new Date(), name: '', email: '', phone: '', address: '' });
    const [buttonLabel, setButtonLabel] = useState('Add');

    const loadCustomers = () => {
        setItems(getCustomers());
    }
    const generateId = () => {
        return items.length + 1;
    }
    const reset = () => {
        setCustomer({
            name: '',
            dob: null,
            email: '',
            phone: '',
            address: '',
            id: 0,
        });
        setButtonLabel("Add");
    }

    const recordDelete = (id) => {
        deleteCustomer({ id });
        loadCustomers();
    }
    const handleChange = (e) => {
        setCustomer({ ...customer, [e.target.name]: e.target.value });
    }
    const doEdit = (id) => {
        let tempArray = items.filter((item) => (item.id == id));
        if (tempArray.length > 0) {
            let { id, dob, name, email, phone, address } = tempArray[0];
            setCustomer({
                id, name, email, phone, address, dob
            });
            setButtonLabel('Update');
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!customer.name.length) {
            return;
        }
        //const newItem = customer;
        const newItem = { ...customer }; //new Object
        if (customer.id != 0) { //edit
            newItem.id = customer.id;
            updateCustomer(newItem);
        } else {//add
            newItem.id = generateId();
            addCustomer(newItem);
        }
        setItems([...getCustomers()]);
        reset();
    }
    const onDateChange = (dob) => {
        setCustomer({ ...customer, dob })
    }
    return (
        <div>
            <h3>Customer App Func</h3>
            <form onSubmit={handleSubmit}>
                <DatePicker
                    placeholder='DoB'
                    name='dob'
                    onChange={onDateChange}
                    value={customer.dob}
                /><br /><br />
                <input
                    placeholder='Name'
                    name='name'
                    onChange={handleChange}
                    value={customer.name}
                /><br /><br />
                <input
                    placeholder='Email'
                    name='email'
                    onChange={handleChange}
                    value={customer.email}
                /><br /><br />
                <input
                    placeholder='Phone'
                    name='phone'
                    onChange={handleChange}
                    value={customer.phone}
                /><br /><br />
                <input
                    placeholder='Address'
                    name='address'
                    onChange={handleChange}
                    value={customer.address}
                /><br /><br />
                <button>{buttonLabel}</button> &nbsp;&nbsp;
                <input type='button' onClick={reset} value="Cancel" />
            </form>
            <CustomerList items={items} onDelete={recordDelete} doEdit={doEdit} />
        </div>
    );
}
const processDateToString =(dateObject)=>{
    if(dateObject == null){
      return "";
    }else{
     return  dateObject.getDate() + "-" + dateObject.getMonth() +"-" + dateObject.getFullYear()+" "
    }
  }
function CustomerList({ items, onDelete, doEdit }) {
    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {items.map(item => (
                        <tr key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.email}</td>
                            <td>{item.phone}</td>
                            <td>{item.address}</td>
                            <td><button onClick={() => {
                                doEdit(item.id);
                            }}>Edit</button></td>
                            <td><button onClick={() => {
                                onDelete(item.id);
                            }} >Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default CustomerFunc;