import React,{useState} from 'react';
import ReactDOM from 'react-dom';
import { useNavigate } from 'react-router';
import { addCustomer, deleteCustomer, getCustomers, updateCustomer } from '../services/CustomerData';


export function CustomerList () {
    const navigate = useNavigate();
    const [items,setItems] = useState(getCustomers());
    const [buttonLabel,setButtonLabel] = useState('Add');
  
  const loadCustomers=()=>{
    setItems(getCustomers());
  }
  const recordDelete=(id)=>{
    deleteCustomer({id});
    loadCustomers();
  }
  const doEdit=(id)=>{
    navigate('/customer/edit/'+id);
    }

    return (
      <div>
        <h3>Customers</h3>
        <button onClick={()=>navigate('/customer/add')}>Add</button><br/><br/>
       <List items={items} onDelete={recordDelete} doEdit={doEdit}/>
      </div>
    );
}

function List ({items,onDelete,doEdit}) {
    return (
        <div>
        <table>
       <thead>
       <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {items.map(item => (
          <tr key={item.id}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td>{item.phone}</td>
            <td>{item.address}</td>
            <td><button onClick={()=>{
                doEdit(item.id);
            }}>Edit</button></td>
            <td><button onClick={()=>{
                onDelete(item.id);
            }} >Delete</button></td>
          </tr>
        ))}
        </tbody>   
        </table>
        </div>
    );
}

export default CustomerList;