import React,{useState} from 'react';
import ReactDOM from 'react-dom';

export function FuncComp () {
    const [items,setItems] = useState([
        {id:1,name:'Vivek',email:'vivek@abc.com',phone:'98982982',address:'India'},
        {id:2,name:'Sunil',email:'sunil@abc.com',phone:'98982982',address:'India'},
    ]);
    const [customer,setCustomer] = useState({id:0, name: '',email:'',phone:'',address:''});
    const [buttonLabel,setButtonLabel] = useState('Add');
  
  const generateId=()=>{
      return items.length +1;
  }
  const reset=()=>{
    setCustomer({
        name: '',
        email: '',
        phone: '',
        address: '',
        id: 0,
      });
    setButtonLabel("Add");
  }

  const recordDelete=(id)=>{
    console.log(">> recordDelete:"+id);
    let tempArray = items.filter((item)=>(item.id !== id));
    setItems(tempArray);
  }
  const handleChange = (e) => {
    console.log("customer",customer)
    setCustomer({...customer, [e.target.name]: e.target.value });
  }

  const doEdit=(id)=>{
    let tempArray = items.filter((item)=>(item.id === id));
    if(tempArray.length > 0){
        let {id,name,email,phone,address} = tempArray[0];
        setCustomer({
            id,name,email,phone,address
        });
        setButtonLabel('Update');
        }
    }

 const handleSubmit = (e) => {
    e.preventDefault();
    if (!customer.name.length) {
      return;
    }
    const newItem = {...customer}
    if(customer.id !== 0){ //edit
        newItem.id = customer.id;
        let tempArray = items.filter((item)=>(item.id === newItem.id));
        if(tempArray.length>0){
            let editRecord = tempArray[0];
            editRecord.name= newItem.name;
            editRecord.email= newItem.email;
            editRecord.address= newItem.address;
            editRecord.phone= newItem.phone;
        }
    }else{//add
        newItem.id = generateId();
        setItems([...items,newItem]);
    }
    reset();
  }
    return (
      <div>
        <h3>Customer App Func</h3>
        <form onSubmit={handleSubmit}>
          <input
            placeholder='Name'
            name='name'
            onChange={handleChange}
            value={customer.name}
          /><br/><br/>
          <input
            placeholder='Email'
            name='email'
            onChange={handleChange}
            value={customer.email}
          /><br/><br/>
          <input
            placeholder='Phone'
            name='phone'
            onChange={handleChange}
            value={customer.phone}
          /><br/><br/>
          <input
            placeholder='Address'
            name='address'
            onChange={handleChange}
            value={customer.address}
          /><br/><br/>
          <button>{buttonLabel}</button> &nbsp;&nbsp;
          <input type='button' onClick={reset} value="Cancel" /> 
        </form>
        <CustomerList items={items} onDelete={recordDelete} doEdit={doEdit}/>
      </div>
    );
}

function CustomerList ({items,onDelete,doEdit}) {
    return (
        <div>
        <table>
       <thead>
       <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {items.map(item => (
          <tr key={item.id}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td>{item.phone}</td>
            <td>{item.address}</td>
            <td><button onClick={()=>{
                doEdit(item.id);
            }}>Edit</button></td>
            <td><button onClick={()=>{
                onDelete(item.id);
            }} >Delete</button></td>
          </tr>
        ))}
        </tbody>   
        </table>
        </div>
    );
}
