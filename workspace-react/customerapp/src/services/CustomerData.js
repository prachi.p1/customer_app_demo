let customers = [
    {id:1,name:'VIvek',email:'vivek@pyther.com',phone:'8364533434',address:'India'},
    {id:2,name:'Amar',email:'amar@pyther.com',phone:'9798763355',address:'India'}
];

 export const getCustomers = ()=>(customers);

 export const addCustomer = (record)=>{
    customers.push(record);
 };

 export const deleteCustomer = (record)=>{
    let tempArray = customers.filter((item)=>(item.id !== record.id));
    customers = tempArray;
};
export const updateCustomer = ({id,name,email,phone,address})=>{
    let tempArray = customers.filter((item)=>(item.id === id));
    if(tempArray.length > 0){
        let editCustomer = tempArray[0];
        editCustomer.name = name;
        editCustomer.email = email;
        editCustomer.phone = phone;
        editCustomer.address = address;
    }
 };
 export const getCustomerById = (id) =>{
    let tempList = customers.filter((item)=>(item.id === id));
    if(tempList.length > 0){
        return tempList[0];
    }else{
        return {};
    }
}

